from os import path

from aws_cdk import (
    Stack,
    aws_dynamodb as dynamo,
    aws_iam as iam,
    aws_kms as kms,
    aws_lambda as lambda_,
    aws_lambda_event_sources as event_sources,
    aws_s3 as s3,
)
from constructs import Construct


class EncryptDynamoStack(Stack):
    def __init__(self, scope: Construct, construct_id: str, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)

        key = kms.Key(self, "DynamoAttributeKey", enable_key_rotation=True)
        key.add_alias("alias/dynamo-attribute-key")

        encrypt_lambda = lambda_.Function(
            self,
            "EncryptDynamoLambda",
            runtime=lambda_.Runtime.PYTHON_3_12,
            handler="index.handler",
            code=lambda_.Code.from_asset(
                path.join(path.dirname(path.dirname(__file__)), "lambda_handler")
            ),
            environment={"DYNAMO_TABLE": "encrypted"},
        )

        encrypt_lambda.add_to_role_policy(
            iam.PolicyStatement(
                actions=["kms:Encrypt", "kms:DescribeKey"],
                effect=iam.Effect.ALLOW,
                resources=[key.key_arn],
            )
        )

        landing_bucket = s3.Bucket(
            self,
            "DynamoLandingBucket",
            block_public_access=s3.BlockPublicAccess.BLOCK_ALL,
            encryption=s3.BucketEncryption.S3_MANAGED,
            enforce_ssl=True,
            versioned=False,
        )

        dynamo_table = dynamo.Table(
            self,
            "DynamoTable",
            table_name="encrypted",
            partition_key=dynamo.Attribute(
                name="id",
                type=dynamo.AttributeType.STRING,
            ),
            billing_mode=dynamo.BillingMode.PAY_PER_REQUEST,
        )

        encrypt_lambda.add_event_source(
            source=event_sources.S3EventSource(
                landing_bucket, events=[s3.EventType.OBJECT_CREATED_PUT]
            )
        )

        landing_bucket.grant_read(encrypt_lambda)
        dynamo_table.grant_read_write_data(encrypt_lambda)
