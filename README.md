# Encrypt Dynamo
This repo is for learning purposes only. It is to demonstrate how to set up a small application created and managed by AWS CDK using Python3. This is an event-driven system that is kicked off by a put event to an S3 bucket. It takes a JSON file and encrypts a sub-set of attributes and puts the encrypted item into a DynamoDB table.

![simple-architecture](docs/image.png)


## Getting started
To stand up this test bed you need to have the following:
AWS Account - (This demo can incur costs view the Price page for each resource you spin up to verify.)
AWS CDK installed
Python > 3.10

## Test

```bash
$ python3 -m pytest
```

## Build and Deploy
```bash
$ cdk synth
$ cdk bootstrap
$ cdk deploy
```
***
