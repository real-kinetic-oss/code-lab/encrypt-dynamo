import json
import os
import boto3
from botocore.exceptions import ClientError

kms_client = boto3.client("kms")
s3_client = boto3.client("s3")
dynamodb = boto3.resource("dynamodb")
table = dynamodb.Table(os.environ.get("DYNAMO_TABLE", "encrypt"))

encrypt_set = {"encrypt", "nested_attribute"}


def handler(event, context):
    s3_inputs = [
        {"Bucket": record["s3"]["bucket"]["name"], "Key": record["s3"]["object"]["key"]}
        for record in event.get("Records")
    ]

    key_response = kms_client.describe_key(KeyId="alias/dynamo-attribute-key")
    key_id = key_response["KeyMetadata"]["KeyId"]

    for s3_input in s3_inputs:
        records = [record for record in get_records(s3_input)]
        encrypted_records = encrypt_records(records, key_id)
        write_records(encrypted_records)

    return 200


def get_records(s3_input: dict[str:str]) -> list[dict]:
    response = s3_client.get_object(
        Bucket=s3_input.get("Bucket"), Key=s3_input.get("Key")
    )
    records = response.get("Body").read().decode("utf-8")
    return [json.loads(record) for record in records.strip().split("\n")]


def write_records(records: list) -> None:
    with table.batch_writer(overwrite_by_pkeys=["id"]) as batch:
        for record in records:
            try:
                batch.put_item(Item=record)
            except ClientError:
                print(f"Failed to put item: {record.get('id')} to dynamo")
                raise


def encrypt_records(records: list[dict], key_id: str) -> list[dict]:
    encrypted_records = []
    for record in records:
        encrypted_records.append(
            {k: encrypt_field(k, v, key_id) for k, v in record.items()}
        )

    return encrypted_records


def encrypt_field(field: str, attribute: str | dict, key_id: str) -> str | dict:
    if isinstance(attribute, dict):
        for k, v in attribute.items():
            return {k: encrypt_field(k, v, key_id)}
    if field in encrypt_set:
        return kms_client.encrypt(
            KeyId=key_id, Plaintext=bytes(attribute, "utf-8")
        ).get("CiphertextBlob")
    return attribute
