import aws_cdk as core
import aws_cdk.assertions as assertions

from encrypt_dynamo.encrypt_dynamo_stack import EncryptDynamoStack


# example tests. To run these tests, uncomment this file along with the example
# resource in encrypt_dynamo/encrypt_dynamo_stack.py
def test_sqs_queue_created():
    app = core.App()
    stack = EncryptDynamoStack(app, "encrypt-dynamo")
    template = assertions.Template.from_stack(stack)

    template.has_resource_properties("AWS::Lambda::Function", {})
